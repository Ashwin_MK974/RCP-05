package tp6;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.*;


public class ChatClient extends JFrame implements ActionListener, Runnable  {

	/*___________ Attributs ______________*/
	private JTextField messageAEnvoyer;		//Zone de saisie du message
	private JButton b_Envoyer;				//Le bouton d'envoi du message
	private Socket maSocket;				//Socket du programme client
	private int numeroPort = 8888;			//Port 
	private String adresseServeur = "localhost";//Adresse du serveur
	private PrintWriter writerClient;		//Objet permettant l'criture de message sur le socket
	private BufferedReader readClient;
	public JTextArea zonetext;
	private BufferedReader readerClient;
	/*___________ Constructeur ______________*/
	public ChatClient()
	{
		//Dfinition de la fentre
		super("Client - Panneau d'affichage");
		setSize(250, 120);
		setLocation(300,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		//Cration des composants graphiques
		//Exercice 3.1 : Modification de l'interface graphique du chat client 
		//pour ajouterune zone daffichage de type JTextArea qui permettra dafficher les messages reus 
		//limage du serveur.
		zonetext = new JTextArea(10,20);
		JScrollPane scrollPane = new JScrollPane(zonetext, 
		JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
		JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		messageAEnvoyer = new JTextField(20);
		b_Envoyer = new JButton("Envoyer");
		b_Envoyer.addActionListener(this);
		zonetext.setEditable(true);
		//Disposition des composants graphiques
		Container pane = getContentPane();
		pane.setLayout(new FlowLayout());
		pane.add(scrollPane,BorderLayout.CENTER);
		pane.add(messageAEnvoyer);
		pane.add(b_Envoyer);
		
		//Ecoute du serveur
		Thread my_thread = new Thread(this);
		my_thread.start();
		/*Creation Socket Client*/
		try
		{
			maSocket = new Socket(adresseServeur, numeroPort);
			writerClient = new PrintWriter(maSocket.getOutputStream()); 
			readClient = new BufferedReader(new InputStreamReader(maSocket.getInputStream()));

		} 
		catch (Exception e) 
		{
			System.out.println("Erreur de cr�ation client");
		}
		setVisible(true);//Rend visible la fenetre	
	}
	/*Fin Creation Socket Client*/
	/*Fin Constructeur*/
	
	
	//Excution du Programme
	public static void main (String[] args)
	{
		new ChatClient();
	}
	//Action du click sur le bouton
	@Override
	public void actionPerformed(ActionEvent click)
	{
		//Clic du bouton Envoyer dclanche la mthode emettre()
		if (click.getSource() == b_Envoyer) 
		{
			notifiy();
			emettre();
			
		}

	}
	//Fin
	/*Mthode Emettre*/
	public void emettre() 
	{
		//Envoi du message
		
		try {
			String message = messageAEnvoyer.getText(); 
			writerClient.println(message); //Envoi du texte par l'objet writer
			writerClient.flush();
			//ecouterServeur(); //Remplacer dans RuN
			messageAEnvoyer.setText("");
			messageAEnvoyer.requestFocus();
			} 
		catch (Exception e) 
		{
			System.out.println("Erreur Envoi du Message");
		}
	}
	/************************Fin Methode EMETRE********************/
	public void zonetxt(String string)
	{
		zonetext.append(string);
	}
	
	public void notifiy()
	{
		zonetext.setText("Vous ete maintenant connect" + "\n");
	}
	//Exercice 2.1 : Ajout de la mthod ecouteServeur
	public void ecouteserveur()
	{
		try 
	{
			zonetxt(" C O N N E C T E R\n");
			readerClient = new BufferedReader(new InputStreamReader(maSocket.getInputStream()));
			String ligne;
			while ((ligne = readerClient.readLine()) != null)
			{ 
				zonetxt( " S E R V E R : " + ligne + "\n");
			}

		} 
		catch (Exception e)
		{
			zonetxt(" Error : Veuillez d'abord excuter ChatServeur");
		}
	}
	//Exercice 3.2 Implmentation de l'interface Runnable
	public void run()
	{
		try 
	{
			zonetxt(" C O N N E C T E R\n");
			readerClient = new BufferedReader(new InputStreamReader(maSocket.getInputStream()));
			String ligne;
			while ((ligne = readerClient.readLine()) != null)
			{ 
				zonetxt( " S E R V E R : " + ligne + "\n");
			}

		} 
		catch (Exception e)
		{
			zonetxt(" Error : Veuillez d'abord excuter ChatServeur");
		}
	}
}
