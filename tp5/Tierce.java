package tp5;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPanel;

public class Tierce extends JFrame implements ActionListener, JugeDeCourse
{
	// Exercice 1.2 Attributs utiliser par la m�thode
	private Vector<Cheval> listecheval;
	//Fin
	public Container panneau;
	//Attributs 1er Element
	public JLabel  resultat;
	//Attribut  Second Element
	public JButton go;
	public int nb =6;
	public int idcheval;
		//Exercice 3 
		public Tierce()
		{
			//Cr�ation de la fen�tre
			super(" Tierce ");
			setSize(600, 200);
			setLocation(400,400);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			//Permet la recuperation du container
			panneau = getContentPane(); 
			//Objet qui sera contenu dans le panneau
			resultat = new JLabel(" R E S U L T A T      ");
			go = new JButton(" G O");
			//Ajout et placement des objet dans le panneau
			panneau.add(resultat, BorderLayout.NORTH);//Resultat au nord
			panneau.add(go, BorderLayout.SOUTH);	 //Bouton go au sud
			setVisible(true); //Afin de pouvoir rendre visible le tableau
			go.addActionListener(this);//rend actif le bouton
			//Vecteur pour stocker les chevaux
			//Exercice 4
			//On dois maintenant implementer juge de course pour gerer l'arriv�e de la course
			listecheval = new Vector<Cheval>(); //cheval contenu dans le vecteur listecheval
			while(idcheval<nb)//Boucle while qui dis tant que le nombre de cheval est inf�rieur � 6
			{
				idcheval++; // On incremente, cela sert pour la creation des chevaux
				Cheval chevaln = new Cheval (idcheval, 50, this); // Creation des chevaux suivant la boucle
				listecheval.addElement(chevaln); //Stockage des chevals crer dans la liste
				//System.out.println(idcheval);
			}
			//Fin Exercice 4
		}
		 public synchronized void passeLaLigneDArrivee(int id)
		 {
			System.out.println("Passe la ligne d'arriv�e " + id); 
			String txt = resultat.getText(); //Recuperation texte deja afficher dans le label resultat
			resultat.setText(txt+ id + "  -  ");//Quand on recuperer on ajoute le nouveau texte, ce qui �vite
												//d'ecraser les ancienne valeurs deja afficher
		 }
		//Fin Exercice 3
		//Exercice 5
		 
		@Override
		public void actionPerformed(ActionEvent click) 
		{
			if (click.getSource() == go) 
			{
				for(int i=0 ; i < listecheval.size();i++)//Pour i allant du debut � la fin de la liste(taille)
														//on incremente
				{
					listecheval.elementAt(i).start();//Pour chacun des passage des  cheval dans listecheval
					//gr�ce a l'indice i (0 � 6) on execute les instruction Contenu dans  run par .start();
				}
			}	
		}
		//Fin Exercice 5
		//Exercice 6: Correction par l'ajout de synchronized
		//Execution du programme
		public static void main(String[] args) 
		{
			Tierce n = new Tierce ();	
		}	
}
