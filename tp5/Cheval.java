package tp5;
//Exercice 2
public class Cheval extends Thread  implements CoureurHippique//Creation de la classe Cheval qui etend de la classe Thread
{
	//Atributs
	public int id;
	private int LongueurCourseAParcourir;
	private JugeDeCourse j; //Juge de course qui devra suivre et juger sa course
	private int position;
	//Constructeur cheval qui prend en compte les 3 param�tre
	public Cheval(int id, int longeurcourseaparcourir, JugeDeCourse jdc)
	{
		this.id = id;
		LongueurCourseAParcourir = longeurcourseaparcourir ;
		j = jdc;
	}
	
	public void run() 
	{
		//Depart � position 0
		position =  0;
		//Avancement du Cheval
		//Tant que la position du cheval n'est pas atteninte, on incremente l'attribut position
		//Exercice 7 : Amelioration du Pas de mani�re al�atoire compris entre 0 et 3
		for (position= 0; position<=LongueurCourseAParcourir;  position +=  (0 +(int)(Math.random()*((3-0)+1))))
		{  
			//On affiche ainsi la position
			System.out.println("Cheval num�ro : "+id+" Position : "+ position +"/"+LongueurCourseAParcourir);
			
			try 
			{
			Thread.sleep(50);//Patiente pendant 50ms
			} catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
			j.passeLaLigneDArrivee(id);	 
	}	
		//Implementation de l'interface CoureurHippique
		@Override
		public int distanceParcourue() 
		{
			return position; //Retourne la position par apport � la distance parcourue
							//Pour permettre de consulter la distance deja parcourue
		}		
}
