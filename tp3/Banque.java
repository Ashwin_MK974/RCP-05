package tp3;

public class Banque {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Execution du programme

		System.out.println("----Exercice 3.2-----");
		Compte c1 = new Compte("TEST");
		c1.consulterSolde();//Consultation du solde de c1
		c1.verserArgent(2); //Versement de 2 euros
		c1.consulterSolde(); //Consultation du nouveau Solde
		System.out.println("---- Fin Exercice 3.2-----");
		System.out.println(" ");
		
		System.out.println("----Exercice 3.3 et 3.4-----");
		Compte c2 = new Compte("Allen"); //Creation du nouveau compte
		VerserPleinArgent v1 = new VerserPleinArgent(c2);
		Thread t1 = new Thread(v1); //Creation d'un nouveau Thread d'ex�cution
		t1.start();					//Ex�cution du Thread
		//Fin Exercice 3.3
		
		//Exercice 3.4 Creation du nouveau Thread qui cible le compte c2
		VerserPleinArgent v2 = new VerserPleinArgent(c2);//Deuxi�me Tache v2 qui cible le compte c2
		Thread t2 = new Thread(v2); //Creation d'un nouveau Thread d'ex�cution
		t2.start();//Ex�cution du Thread
		//Fin Exercice 3.4	
		//Bien que th�oriquement le compte devrait se retoruver avec un solde de 400 �
		//On approche un resultat de 396 euros
		//Exercice 3.6
	}

}
