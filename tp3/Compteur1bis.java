package tp3;

public class Compteur1bis implements Runnable {

	/*Exercice 3.4
		Classe Compteur1bis qui fais la m�me activit� mais en implementant l'interface Runnable
	 */
	private String nom;
	public Compteur1bis(String nom) {
		this.nom=nom;
	}
	public void run() 
	{
		for (int i =1; i< 1000; i++) System.out.print(i + " " + nom );
	}
	public static void  main(String args[]) throws InterruptedException{
		Compteur1bis t1, t2, t3;
		t1=new Compteur1bis("Hello ");
		t2=new Compteur1bis("World ");
		t3=new Compteur1bis("and Everybody ");
		Thread t1b = new Thread(t1);	//Ajoute le role du compteur au Thread
		t1b.start();
		Thread t2b = new Thread(t2);	//Ajoute le role du compteur au Thread
		t2b.start();
		Thread t3b = new Thread(t3);	//Ajoute le role du compteur au Thread
		t3b.start();
		Thread.sleep(100);
		System.out.println("Fin du programme");
		System.exit(0);
	}

}