package tp3;

public class Compteur1 extends Thread implements Runnable 
{
	/*Exercice 1.1
	 Lors de l'ex�cution du Compteur1.java on constate que le nombre 
	d'affichage ne correspondent pas au nombre d'affichage souhaiter.
	System.exit(10) va arreter le programme principale c'est-�-dire tous
	les thread qui s'y trouvent � l'int�rieur
	 */
	/*Exercice 1.2 
	En ajoutant une pause de 100 ms on constate que les Thread on plus de temps
	pour  ex�cuter les t�ches qui leurs sont assign�es.
	 */
	/*Exercice 1.3
 	En ajoutant une pause de 100 ms cela permet  � chaque de Thread 
 	d'effectuer les tache qui leurs sont assign� succesivement 
 	ce qui explique l'ordre d'affichage
	 */
	private String nom;
	public Compteur1(String nom)
	{
	this.nom=nom;
	}
	public void run()
	{
		for (int i =1; i< 1000; i++) System.out.print(i + " " + nom );
	}
	public static void  main(String args[]) throws InterruptedException{
		Compteur1 t1, t2, t3;
		t1=new Compteur1("Hello ");
		t2=new Compteur1("World ");
		t3=new Compteur1("and Everybody ");

		t1.start();
		t2.start();
		t3.start();
		Thread.sleep(100);
		System.out.println("Fin du programme");
		System.exit(0);
		
		Compteur1 c1bis= new Compteur1("Test");
		Thread my_thread = new Thread(c1bis);	//Ajoute le role du compteur au Thread
		my_thread.start();
	}
}
