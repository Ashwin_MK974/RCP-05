package tp3;
public class VerserPleinArgent  implements Runnable
{
	//Exercice 3.3
	public Compte monCompte;
	//Constructeur qui prend en param�tre un compte
	public VerserPleinArgent (Compte c) 
	{
		monCompte =  c;
	}
	@Override
	public void run()
	{
		//M�thode qui verse 100 fois 2 euros sur le compte
		//Pour i allant de 1 � 100 on verse 2 euros et on affiche le solde de suite
		for(int i=1;i<100;i++)
		{
			monCompte.verserArgent(2);//Versement de 0 euros
			monCompte.consulterSolde();//Affichage du solde
			try 
			{			
				Thread.sleep(50);//Thread.sleep(50) permet d'effectuer une pause de
								 //50ms entre chaque versement
			} 
			catch (InterruptedException c) 
			{}
		}
		//Fin Exercice 3.3
}
}
