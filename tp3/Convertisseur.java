package tp3;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPanel;

public class Convertisseur extends JFrame implements ActionListener
{
	// Exercice 1.2 Attributs utiliser par la m�thode
	public double displayttc; //Contiendra les r�sultat de la formule TTC = HT*(tva/100)+ ht
	//Fin
	public Container panneau;
	//Attributs 1er Pannel
	public JLabel  montant_tva;
	public JTextField insert_tva;
	//Attributs second Pannel
	public JLabel montant_ht;
	public JTextField insert_ht;
	public JLabel  montant_ttc;
	public JTextField displays_ttc; //affichage de TTC
	//Attribut du dernier pannel
	public JButton calculer;

	public Convertisseur()
	{
		//Cr�ation de la fen�tre
		super(" Convertisseur ");
		setSize(600, 200);
		setLocation(400,400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//Permet la recuperation du container
		panneau = getContentPane();
	    panneau.setLayout(new GridLayout(3,3));
	    
		//Creation de  3 pannels afin de contenir les �l�ments
		JPanel first = new JPanel(); //Premiere ligne
		JPanel middle = new JPanel();//deuxi�me ligne
		JPanel ended = new JPanel();//Troisi�me ligne	
		/*-------Premier Pannel conteant des elements-----*/
		 montant_tva = new JLabel(" Montant TVA ");
		 insert_tva = new JTextField(" ");
		/*-------Fin-----*/			
		/*------Deuxi�me Pannel conteant des elements-----*/			
		 montant_ht = new JLabel(" Montant HT ");
		 insert_ht =  new JTextField("");
		 montant_ttc = new JLabel(" Montant TTC ");
		 displays_ttc =  new JTextField(""); //displays_ttc afffiche ici la variable displayttc
		/*-------Fin-----*/
        /*------Troisi�me Pannel conteant 1 element-----*/
		 calculer = new JButton(" C A L C U L E R ");
		/*-------Fin-----*/			
		first.setLayout(new GridLayout(0, 2));//Ajout de 2 �l�ments en haut
		first.add(montant_tva);				  
		first.add(insert_tva);
		
		middle.setLayout(new GridLayout(0, 4)); //Ajout de 4 �l�ments au millieu
		middle.add(montant_ht);
		middle.add(insert_ht);
		middle.add(montant_ttc);
		middle.add(displays_ttc);

		ended.setLayout(new GridLayout(0, 1));//Ajout de 1 element touts en bas qui prend tous l'espace dispo
		ended.add(calculer);
		
		//Ajout des grids c'est-�-dire Label cr�er pr�cedement aux panneaux (Conteneur)
		panneau.add(first);
		panneau.add(middle);
		panneau.add(ended);
		calculer.addActionListener(this);//Cela permet de detecter le click utile pour la condition
										 //if(click.getsource() == calculer) sinon l'�venement n'est pas detecter
		setVisible(true); //Afin de pouvoir rendre visible le tableau
		}
		public void versTTC( Double tva, Double ht)
		{
				tva = Double.parseDouble(insert_tva.getText()); //Recupere la valeur entrer dans TVA
				ht = Double.parseDouble(insert_ht.getText());  //Recupere la valeur entrer dans HT
				displayttc = ht*(tva/100) + ht; //Effectue l'op�ration de calcul de ttc et le stocke dans displayttc
				displays_ttc.setText("" + displayttc);
				//On affiche ainsi displayttc qui est le resultat de la formule  TTC = HT*(tva/100)	+ ht
		}
		@Override
		public void actionPerformed(ActionEvent click) 
		{
			//Si on click sur le bouton calculer
			if (click.getSource() == calculer) 
			{	//On recupere les valeur contenu dans les champs insert_tva et insert_ht
				//pour pouvoir ex�cuter la m�thode
				versTTC(Double.parseDouble(insert_tva.getText()), Double.parseDouble(insert_ht.getText())) ;
			}
		}
		//Execution du programme
		public static void main(String[] args) 
		{
			Convertisseur n = new Convertisseur ();	
		}	
}
